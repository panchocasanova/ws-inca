<?php 
/**
 * 
 */
class db 
{	
	private $host = 'localhost';
	private $usuario = 'root';
	private $password = '';
	private $database = 'bd_listaespera_pruebas';

	//conectar a bd
	public function conectar(){
		$conexion_mysql = "mysql:host=$this->host;dbname=$this->database";
		$conexionDB = new PDO($conexion_mysql, $this->usuario, $this->password);
		$conexionDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		//esta linea arregla la codificacion de caracteres utf8
		$conexionDB->exec("set names utf8");
		return $conexionDB;
	}
}

?>
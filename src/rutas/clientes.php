<?php 
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;
date_default_timezone_set('America/Santiago');
//obtener todos los clientes
$app->get('/api/ws', function(Request $request, Response $response){
	$consulta = 'SELECT * FROM t_solicitud';
	try{
		$db = new db();
		$db = $db->conectar();
		$ejecutar = $db->query($consulta);
		$clientes = $ejecutar->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		//echo json_encode($clientes);

		if($clientes) {
            return $response->withStatus(200)
        			->withHeader('Content-Type', 'application/json')
        			->write(json_encode($clientes));

        } else { throw new PDOException('No records found');}

	}catch(PDOException $e){
		echo '{"Error": {"text":'.$e->getMessage().' }';
	}
});

//obtener un cliente
$app->get('/api/ws/{id}', function(Request $request, Response $response){
	$id= $request->getAttribute('id');
	$consulta = 'SELECT * FROM t_solicitud where id ='. $id;
	try{
		$db = new db();
		$db = $db->conectar();
		$ejecutar = $db->query($consulta);
		$cliente = $ejecutar->fetchAll(PDO::FETCH_OBJ);
		$db = null;

		if($cliente) {
            return $response->withStatus(200)
        			->withHeader('Content-Type', 'application/json')
        			->write(json_encode($cliente));

        } else { throw new PDOException('No records found');}

	}catch(PDOException $e){
		echo '{"Error": {"text":'.$e->getMessage().' }';
	}
});

//agregar un cliente
$app->post('/api/ws/new', function(Request $request, Response $response){
	//$id = $request->getParam('id');
	$numreg = $request->getParam('numreg');
	$run = $request->getParam('run');
	$dv = $request->getParam('dv');
	$nombre = $request->getParam('nombre');
	$primer_apellido = $request->getParam('primer_apellido');
	$segundo_apellido = $request->getParam('segundo_apellido');
	$fecha_nac = $request->getParam('fecha_nac');
	$sexo = $request->getParam('sexo');
	$prevision	 = $request->getParam('prevision');
	$prais = $request->getParam('prais');
	$region = $request->getParam('region');
	$ciudad = $request->getParam('ciudad');
	$comuna = $request->getParam('comuna');
	$cond_ruralidad = $request->getParam('cond_ruralidad');
	$via_direccion = $request->getParam('via_direccion');
	$nom_calle = $request->getParam('nom_calle');
	$num_direccion = $request->getParam('num_direccion');
	$resto_direccion = $request->getParam('resto_direccion');
	$fono_fijo = $request->getParam('fono_fijo');
	$fono_movil = $request->getParam('fono_movil');
	$email = $request->getParam('email');
	$sospecha_diag = $request->getParam('sospecha_diag');
	$confir_diag = $request->getParam('confir_diag');
	$ficha = $request->getParam('ficha');
	$f_entrada = $request->getParam('f_entrada');
	$f_citacion = $request->getParam('f_citacion');
	$run_prof_sol = $request->getParam('run_prof_sol');
	$dv_prof_sol = $request->getParam('dv_prof_sol');
	$estab_orig = $request->getParam('estab_orig');
	$estab_dest = $request->getParam('estab_dest');
	$tipo_prest = $request->getParam('tipo_prest');
	$presta_min = $request->getParam('presta_min');
	$presta_est = $request->getParam('presta_est');
	$plano = $request->getParam('plano');
	$extremidad = $request->getParam('extremidad');
	$estado = $request->getParam('estado');
	$fecha = date('Y-m-d h:i:s');
	



	$consulta = 'INSERT INTO t_solicitud (numreg, run, dv, nombres, primer_apellido, segundo_apellido, fecha_nac, sexo, prevision, prais, region, ciudad, cond_ruralidad, via_direccion, nom_calle, num_direccion, resto_direccion, fono_fijo, fono_movil, email, sospecha_diag, confir_diag, ficha, f_entrada, f_citacion, run_prof_sol, dv_prof_sol, estab_orig, estab_dest, tipo_prest, presta_est, plano, extremidad, estado, fecha) VALUES(:numreg, :run, :dv, :nombres, :primer_apellido, :segundo_apellido, :fecha_nac, :sexo, :prevision, :prais, :region, :ciudad, :cond_ruralidad, :via_direccion, :nom_calle, :num_direccion, :resto_direccion, :fono_fijo, :fono_movil, :email, :sospecha_diag, :confir_diag, :ficha, :f_entrada, :f_citacion, :run_prof_sol, :dv_prof_sol, :estab_orig, :estab_dest, :tipo_prest, :presta_est, :plano, :extremidad, :estado, :fecha)';

	try{
		$db = new db();
		$db = $db->conectar();
		$statment = $db->prepare($consulta);
		$statment->bindParam(':numreg', $numreg);
		$statment->bindParam(':run', $run);
		$statment->bindParam(':dv', $dv);
		$statment->bindParam(':nombres', $nombres);
		$statment->bindParam(':primer_apellido', $primer_apellido);
		$statment->bindParam(':segundo_apellido', $segundo_apellido);
		$statment->bindParam(':fecha_nac', $fecha_nac);
		$statment->bindParam(':sexo', $sexo);
		$statment->bindParam(':sexo', $sexo);
		$statment->bindParam(':prevision', $prevision);
		$statment->bindParam(':prais', $prais);
		$statment->bindParam(':region', $region);
		$statment->bindParam(':ciudad', $ciudad);
		$statment->bindParam(':cond_ruralidad', $cond_ruralidad);
		$statment->bindParam(':via_direccion', $via_direccion);
		$statment->bindParam(':nom_calle', $nom_calle);
		$statment->bindParam(':num_direccion', $num_direccion);
		$statment->bindParam(':resto_direccion', $resto_direccion);
		$statment->bindParam(':fono_fijo', $fono_fijo);
		$statment->bindParam(':fono_movil', $fono_movil);
		$statment->bindParam(':email', $email);
		$statment->bindParam(':sospecha_diag', $sospecha_diag);
		$statment->bindParam(':confir_diag', $confir_diag);
		$statment->bindParam(':ficha', $ficha);
		$statment->bindParam(':f_entrada', $f_entrada);
		$statment->bindParam(':f_citacion', $f_citacion);
		$statment->bindParam(':run_prof_sol', $run_prof_sol);
		$statment->bindParam(':dv_prof_sol', $dv_prof_sol);
		$statment->bindParam(':estab_orig', $estab_orig);
		$statment->bindParam(':estab_dest', $estab_dest);
		$statment->bindParam(':tipo_prest', $tipo_prest);
		$statment->bindParam(':presta_est', $presta_est);
		$statment->bindParam(':plano', $plano);
		$statment->bindParam(':extremidad', $extremidad);
		$statment->bindParam(':estado', $estado);
		$statment->bindParam(':fecha', $fecha);
		

		if($statment->execute()){
			echo '{"notice":{"text:":"cliente agregado"}}';
		}


		//echo json_encode($cliente);

	}catch(PDOException $e){
		echo '{"Error": {"text":'.$e->getMessage().' }';
	}
});
/*
//actualizar un cliente
$app->put('/api/clientes/update/{id}', function(Request $request, Response $response){
	

	
	$id= $request->getAttribute('id');
	$nombres = $request->getParam('nombres');
	$apellidos = $request->getParam('apellidos');
	$telefono = $request->getParam('telefono');
	$email = $request->getParam('email');
	$direccion = $request->getParam('direccion');
	$ciudad = $request->getParam('ciudad');
	$departamento = $request->getParam('departamento');
	$consulta = 'UPDATE clientes SET
					nombres = :nombres,
					apellidos = :apellidos,
					telefono = :telefono,
					email = :email,
					direccion = :direccion,
					ciudad = :ciudad,
					departamento = :departamento
					WHERE id ='.$id;

	try{
		$db = new db();
		$db = $db->conectar();
		$statment = $db->prepare($consulta);
		$statment->bindParam(':nombres', $nombres);
		$statment->bindParam(':apellidos', $apellidos);
		$statment->bindParam(':telefono', $telefono);
		$statment->bindParam(':email', $email);
		$statment->bindParam(':direccion', $direccion);
		$statment->bindParam(':ciudad', $ciudad);
		$statment->bindParam(':departamento', $departamento);

		if($statment->execute()){
			echo '{"notice":{"text":"cliente actualizado"}}';
		}else{
			echo '{"notice":{"text":"cliente no actualizado"}}';
		}


		//echo json_encode($cliente);

	}catch(PDOException $e){
		echo '{"Error": {"text":'.$e->getMessage().' }';
	}
});*/

?>